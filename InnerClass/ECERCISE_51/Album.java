package assignments.InnerClass.ECERCISE_51;

import java.util.ArrayList;
import java.util.List;

public class Album {
  private String name, artist;
  private SongList songs;
  public Album(String name, String artist){
    this.name = name;
    this.artist = artist;
    this.songs = new SongList();
  }

  public boolean addSong(String title, double duration){
    return this.songs.add(new Song(title, duration));
  }

  public boolean addToPlaylist(int idx, List<Song> playlist){
    Song song = this.songs.findSong(idx);
    if(song == null){
      System.out.println("This album does not have a track " + idx);
      return false;
    }
    playlist.add(song);
    return true;
  }

  public boolean addToPlaylist(String title, List<Song> playlist){
    Song song = this.songs.findSong(title);
    if(song == null){
      System.out.println("The song " + title + " is not in this album");
      return false;
    }
    this.songs.add(song);
    return true;
  }

  private class SongList {
    private ArrayList<Song> songs;
    public  SongList(){
      this.songs = new ArrayList<Song>();
    }
    public boolean add(Song song){
      if(songs.contains(song)){
        return false;
      }
      this.songs.add(song);
      return true;
    }
    public Song findSong(int idx){
      if(idx - 1 >= 0 && idx - 1 < this.songs.size()){
        return this.songs.get(idx - 1);
      }
      return null;
    }
    public Song findSong(String title){
      for(Song song : this.songs){
        if(song.getTitle().equals(title)){
          return song;
        }
      }
      return null;
    }
  }
}
