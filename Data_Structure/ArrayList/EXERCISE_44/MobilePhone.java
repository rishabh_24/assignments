package assignments.Data_Structure.ArrayList.EXERCISE_44;

import java.util.ArrayList;

public class MobilePhone {
  private String myNumber;
  private ArrayList<Contact> myContacts;
  public MobilePhone(String myNumber){
    this.myNumber = myNumber;
    this.myContacts = new ArrayList<Contact>();
  }
  public boolean addNewContact(Contact x){
    for(Contact i : myContacts){
      if(x.getName().equals(i.getName())){
        return false;
      }
    }
    this.myContacts.add(x);
    return true;
  }
  public boolean updateContact(Contact old, Contact newContact){
    if(!this.myContacts.contains(old)){
      return false;
    }
    int idx = this.myContacts.indexOf(old);
    this.myContacts.add(idx, newContact);
    return true;
  }
  public boolean removeContact(Contact x){
    if(!this.myContacts.contains(x)){
      return false;
    }
    this.myContacts.remove(x);
    return true;
  }
  private int findContact(Contact x){
    return this.myContacts.indexOf(x);
  }
  private int findContact(String str){
    for(Contact c : this.myContacts){
      if(c.getName().equals(str)){
        return this.myContacts.indexOf(c);
      }
    }
    return -1;
  }
  public Contact queryContact(String str){
    for(Contact c : this.myContacts){
      if(c.getName().equals(str)){
        return c;
      }
    }
    return null;
  }
  public void printContact(){
    System.out.println("Contact List:");
    int idx = 1;
    for(Contact c : this.myContacts){
      System.out.println(idx + " " + c.getName() + " -> " + c.getPhoneNumber());
      idx++;
    }
  }
}