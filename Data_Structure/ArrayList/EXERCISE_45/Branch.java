package assignments.Data_Structure.ArrayList.EXERCISE_45;

import java.util.ArrayList;

public class Branch {
  private ArrayList<Customer> customers;
  private String branchName;

  public Branch(String name){
    this.branchName = name;
    this.customers = new ArrayList<Customer>();
  }

  public String getName(){
    return branchName;
  }

  public ArrayList<Customer> getCustomers(){
    return customers;
  }

  public boolean newCustomer(String customerName, double initialAmount){
    if(findCustomer(customerName) != null){
      return false;
    } 
    Customer customer = new Customer(customerName, initialAmount);
    this.customers.add(customer);
    return true;
  }

  public boolean addCustomerTransaction(String customerName, double amount){
    Customer existingCustomer = findCustomer(customerName);
    if(existingCustomer != null){
      existingCustomer.addTransaction(amount);
      return true;
    }
    return false;
  }

  private Customer findCustomer(String customerName){
    for(Customer entity : this.customers){
      if(entity.getName().equals(customerName)){
        return entity;
      }
    }
    return null;
  }
}
