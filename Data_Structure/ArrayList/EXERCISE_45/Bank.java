package assignments.Data_Structure.ArrayList.EXERCISE_45;

import java.util.ArrayList;

public class Bank {
  private String name;
  private ArrayList<Branch> branches;

  public Bank(String name){
    this.name = name;
    this.branches = new ArrayList<Branch>();
  }

  public boolean addBranch(String branchName){
    if(findBranch(branchName) == null){
      this.branches.add( new Branch(branchName));
      return true;
    }
    return false;
  }

  public boolean addCustomer(String branchName, String customerName, double initialAmount){
    Branch branch = findBranch(branchName);
    if(branch != null){
      return branch.newCustomer(customerName, initialAmount);
    }
    return false;
  }

  public boolean addCustomerTransaction(String branchName, String customerName, double amount){
    Branch branch = findBranch(branchName);
    if(branch != null){
      return branch.addCustomerTransaction(customerName, amount);
    }
    return false;
  }

  private Branch findBranch(String branchName){
    for(Branch entity : branches){
      if(entity.getName().equals(branchName)){
        return entity;
      }
    }
    return null;
  }
  public boolean listCustomers(String branchName, boolean showTransactions){
    Branch branch = findBranch(branchName);
    int idx = 1;
    if(branch != null){
      System.out.println("Customers details for branch " + branch.getName());
      ArrayList<Customer> customers = branch.getCustomers();
      for(Customer entityCustomer : customers){
        System.out.println("Customer: " + entityCustomer.getName() + "[" + idx + "]");
        idx++;
        if(showTransactions){
          int index = 1;
          ArrayList<Double> transactions = entityCustomer.getTransactions();
          System.out.println("Transactions");
          for(Double amount : transactions){
            System.out.println("[" + index +"] " + "Amount " + amount);
            index++;
          }
        }
      }
      return true;
    }
    return false;
  }

}
