package assignments.Data_Structure.Array.EXERCISE_43;

import java.util.Arrays;

public class ReverseArray {
  public static void Reverse(int[] arr){
    int i = 0, j = arr.length - 1;
    String strA = Arrays.toString(arr);
    System.out.println("Array = "+strA);
    while (i < j){
      arr[i] = arr[i] ^ arr[j];
      arr[j] = arr[j] ^ arr[i];
      arr[i] = arr[i] ^ arr[j];
      i++; j--;
    }
    String stringArr = Arrays.toString(arr);
    System.out.println("Reversed array = "+stringArr);
  }
}
