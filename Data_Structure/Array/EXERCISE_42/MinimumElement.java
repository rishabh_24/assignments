package assignments.Data_Structure.Array.EXERCISE_42;

import java.util.Scanner;

public class MinimumElement {
  private static int readInteger(){
    Scanner sc = new Scanner(System.in);
    int result = sc.nextInt();
    sc.close();
    return result;
  }
  private static int[] readElements(int num){
    int[] arr = new int[num];
    Scanner sc = new Scanner(System.in);
    for(int i = 0; i < arr.length; i++){
      arr[i] = sc.nextInt();
    }
    sc.close();
    return arr;
  }
  public static int findMin(int[] arr){
    int mn = Integer.MAX_VALUE;
    for(int i = 0; i < arr.length; i++){
      mn = Math.min(mn, arr[i]);
    }
    return mn;
  }
}
