package assignments.Data_Structure.Array.EXERCISE_41;

import java.util.Arrays;
import java.util.Scanner;

public class SortedArray {
  private static Scanner scanner = new Scanner(System.in);
  public static int[] getIntegers(int num){
    int[] arr = new int[num];
    for(int i = 0; i < num; i++){
      arr[i] = scanner.nextInt();
    }
    scanner.close();
    return arr;
  }
  public static void printArray(int[] arr){
    for(int i = 0; i < arr.length; i++){
      System.out.println("Element "+ i + " contents " + arr[i]);
    }
  }
  public static int[] sortIntegers(int[] arr){
    Arrays.sort(arr);
    int i = 0, j = arr.length - 1;
    while(i < j){
      arr[i] = arr[i] ^ arr[j];
      arr[j] = arr[j] ^ arr[i];
      arr[i] = arr[i] ^ arr[j];
      i++; j--;
    }
    return arr;
  }
}
