package assignments.Data_Structure.LinkedList.EXERCISE_46;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Album {
  private String name, artist;
  private ArrayList <Song> songs;

  public Album(String name, String artist){
    this.name = name;
    this.artist = artist;
    this.songs = new ArrayList<Song>();
  }

  public boolean addSong(String title, double duration){
    Song exist = findSong(title);
    if(exist == null){
      Song newSong = new Song(title, duration);
      songs.add(newSong);
      return true;
    }
    return false;
  }
  public boolean addToPlaylist(int idx, LinkedList<Song> list){
    if(idx >= 1 && idx < songs.size()){
      list.add(songs.get(idx - 1));
      return true;
    }
    return false;
  }
  public boolean addToPlaylist(String title, LinkedList<Song> list){
    Song exist = findSong(title);
    if(exist != null){
      songs.add(exist);
      return true;
    }
    return false;
  }
  private Song findSong(String title){
    for(Song song : songs){
      if(song.getTitle().equals(title)){
        return song;
      }
    }
    return null;
  }
}
