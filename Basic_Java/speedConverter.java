package assignments.Basic_Java;

public class speedConverter {
  public static long toMilesPerHour(double kmph){
    if(kmph < 0){
      return -1;
    }
    return Math.round(kmph / 1.609);
  }
  public static void printConversion(double kmph){
    if(kmph < 0){
      System.out.println("Invalid Value");
    } else {
      long miph = toMilesPerHour(kmph);
      System.out.println(kmph + " km/h = " + miph + " mi/h");
    }
    return;
  }
  public static void main(String[] args){
    double kmph = 10.5d;
    printConversion(kmph);
  }
}
