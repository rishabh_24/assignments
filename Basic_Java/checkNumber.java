package assignments.Basic_Java;

public class checkNumber {
  public static void sign(int num){
    String ans;
    if(num > 0){
      ans = "positive";
    } else if(num < 0){
      ans = "negative";
    } else{
      ans = "equal to zero";
    }
    System.out.println(ans);
  }
  public static void main(String[] arg){
    int num = 8;
    sign(num);
    return;
  }
}
