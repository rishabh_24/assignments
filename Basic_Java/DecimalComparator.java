package assignments.Basic_Java;

public class DecimalComparator {
  public static boolean areEqualByThreeDecimalPlaces(double a,  double b){
    int x = (int)(a * 1000);
    int y = (int)(b * 1000);
    return x == y;
  }
  public static void main(String[] a){
    System.out.println(areEqualByThreeDecimalPlaces(-1.1756, -1.175));
  }
}
