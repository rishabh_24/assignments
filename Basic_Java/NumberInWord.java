package assignments.Basic_Java;

public class NumberInWord {
  public static void printNumberInWord(int num){
    String s;
    switch(num){
      case 0:
        s = "ZERO";
        break;
      case 1:
        s = "ONE";
        break;
      case 2:
        s = "TWO";
        break;
      case 3:
        s = "THREE";
        break;
      case 4:
        s = "FOUR";
        break;
      case 5:
        s = "FIVE";
        break;
      case 6:
        s = "SIX";
        break;
      case 7:
        s = "SEVEN";
        break;
      case 8:
        s = "EIGHT";
        break;
      case 9:
        s = "NINE";
        break;
      default:
        s = "OTHER";
        break;
     }
    System.out.println(s);
  }
}
