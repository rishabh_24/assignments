package assignments.Basic_Java;

public class SecondToMinute {
  public static String getDurationString(long minutes, long second){
    if(minutes < 0 || (second < 0 || second > 59)){
      return  "Invalid value";
    }
    String Hours = "", Minutes = "", Seconds = "" + second;
    long hours = minutes  / 60;
    long remMinutes = minutes % 60;
    if(hours < 10){
     Hours = "0" + hours;
    }
    if(remMinutes < 10){
      Minutes = "0" + remMinutes;
    }
    if(second < 10){
      Seconds = "0"  + second;
    }
    return Hours + "h " + Minutes + "m " + Seconds + "s";
  }
  public static void main(String[] args){
    System.out.println(getDurationString(65, 45));
  }
}
