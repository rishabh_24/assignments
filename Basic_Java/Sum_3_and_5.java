package assignments.Basic_Java;

public class Sum_3_and_5 {
  public static void main(String[] args) {
    int lower = 1 + (int)(50 * Math.random()), cnt = 0, sum = 0;
    int upper = lower + (int)(949 * Math.random());
    for(int i = lower; i <= upper && cnt < 5; i++) {
      if((i % 3 == 0) && (i % 5 == 0)){
        cnt++;
        sum += i;
        System.out.println(i);
      }
    }
    System.out.println(sum);
  }
}
