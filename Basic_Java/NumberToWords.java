package assignments.Basic_Java;

public class NumberToWords {
  public static int reverse(int num){
    int trans = 0;
    while(num != 0){
      trans = trans * 10 + num % 10;
      num /= 10;
    }
    return trans;
  }
  public static int getDigitCount(int num){
    if(num < 0){
      return -1;
    }
    if(num == 0){
      return 1;
    }
    int cnt = 0;
    while(num != 0){
      num /= 10;
      cnt++;
    }
    return cnt;
  }
  public static void numberToWords(int num){
    if(num < 0){
      System.out.println("Invalid Value");
      return;
    }
    int cnt = getDigitCount(num);
    int rev = reverse(num);
    int cntRev = getDigitCount(rev);
    int left = cnt - cntRev;
    boolean flag = false;
    String s = "";
    while(rev != 0){
      flag = true;
      int temp = rev % 10;
      if (temp == 0) {
        s += (rev - rev % 10 > 0) ? "Zero " : "Zero ";
      } else if (temp == 1) {
        s += (rev - rev % 10 > 0) ? "One " : "One ";
      } else if (temp == 2) {
        s += (rev - rev % 10 > 0) ? "Two " : "Two ";
      } else if (temp == 3) {
        s += (rev - rev % 10 > 0) ? "Three " : "Three ";
      } else if(temp == 4) {
        s += (rev - rev % 10 > 0) ? "Four " : "Four ";
      } else if (temp == 5) {
        s += (rev - rev % 10 > 0) ? "Five " : "Five ";
      } else if (temp == 6) {
        s += (rev - rev % 10 > 0) ? "Six " : "Six ";
      } else if (temp == 7) {
        s += (rev - rev % 10 > 0) ? "Seven " : "Seven ";
      } else if (temp == 8) {
        s += (rev - rev % 10 > 0) ? "Eight " : "Eight ";
      } else  {
        s += (rev - rev % 10 > 0) ? "Nine " : "Nine ";
      }
      rev /= 10;
      cntRev--;
    }
    if(!flag){
      System.out.println("Zero");
      return;
    }
    while(left != 0){
      s += (left > 1) ? "Zero " : "Zero ";
      left--;
    }
    System.out.println(s);
    return;
  }
  public static void main(String[] args) {
    int num = (int)(10000 * Math.random());
    System.out.println(num);
    numberToWords(10);
    return;
  }
}
