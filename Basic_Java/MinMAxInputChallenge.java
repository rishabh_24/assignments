package assignments.Basic_Java;

import java.util.Scanner;

public class MinMAxInputChallenge {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    while(true){
      boolean isInt = scanner.hasNextInt();
      if(isInt){
        int num = scanner.nextInt();
        min = Math.min(min, num);
        max = Math.max(max, num);
      } else {
        break;
      }
    }
    scanner.close();
    System.out.println("Min: " + min + " Max: " + max);
    return;
  }
}
