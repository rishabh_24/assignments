package assignments.Basic_Java;
public class IntEqualityPrinter {
  public static void printEqual(int a, int b, int c){
    if(a < 0 || b < 0 || c < 0){
      System.out.println("Invalid Value");
    } else if((a + b) == (2 * c)){
      System.out.println("All numbers are equal");
    } else if((a == b && a != c) || ((a == c) && a != b) || ((b == c) && b != a)){
      System.out.println("Neither all are equal or different");
    } else {
      System.out.println("All numbers are different");
    }
    return;
  }
}
