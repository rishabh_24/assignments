package assignments.Basic_Java;

public class SumDigits {
  public static void main(String[] args) {
    int num = (int)(Math.random() * 1000), digSum = 0;
    System.out.println(num);
    if(num <= 9){
      System.out.println(-1);
    } else {
      do {
        digSum += num % 10;
        num /= 10;  
      } while(num != 0);
    }
    System.out.println(digSum);
  }
}
