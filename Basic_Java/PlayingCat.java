package assignments.Basic_Java;
public class PlayingCat {
  public static boolean isCatPlaying(boolean isSummer, int temp){
    if(isSummer && (temp >= 25 && temp <= 45)){
      return true;
    }
    if(!isSummer && (temp >= 25 && temp <= 35)){
      return true;
    }
    return false;
  }
}
