package assignments.Basic_Java;

import java.util.Scanner;

public class ReadUserInput {
  public static void main(String[] args) {
    int sum = 0;
    Scanner scanner = new Scanner(System.in);
    for(int i = 0; i < 10; i++){
      boolean isInt = scanner.hasNextInt();
      if(isInt){
        int num = scanner.nextInt();
        sum += num;
      } else {
        System.out.println("Invalid Number");
      }
      scanner.nextLine();
    }
    System.out.println(sum);
    scanner.close();
    return;
  }
}
