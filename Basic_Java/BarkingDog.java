package assignments.Basic_Java;

public class BarkingDog {
  public static boolean shouldWakeUp(boolean isBarking, int hrs){
    if(isBarking && ((hrs >= 0 && hrs < 8) || (hrs > 22 && hrs <= 23))){
      return true;
    }
    return false;
  }
}
