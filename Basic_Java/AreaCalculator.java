package assignments.Basic_Java;

public class AreaCalculator {
  public static double area(double a){
    if(a < 0){
      return -1;
    }
    return Math.PI * a * a;
  }
  public static double area(double a, double b){
    if(a < 0 || b < 0){
      return -1;
    }
    return a * b;
  }
  public static void main(String[] args) {
    System.out.println(area(5.0));
    System.out.println(area(-1));
    System.out.println(area(5.0, 4.0));
    System.out.println(area(-1.0, 4.0));
    return;
  }
}
