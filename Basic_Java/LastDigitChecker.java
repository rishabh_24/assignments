package assignments.Basic_Java;

public class LastDigitChecker {
  public static boolean inRange(int num){
    return num >= 10 && num <= 1000;
  }
  public static int lastDigit(int num){
    return num % 10;
  }
  public static boolean hasSameLastDigit(int value1, int value2, int value3) {
    if(!inRange(value1) || !inRange(value2) || !inRange(value3)){
      return false;
    }
    int last1 = lastDigit(value1), last2 = lastDigit(value2), last3 = lastDigit(value3);
    if(last1 == last2 && last3 == last2){
      return true;
    }
    if((last1 == last2 && last3 != last2) || (last1 == last3 && last2 != last3) || (last2 == last3 && last3 != last1)){
      return true;
    } 
    return false;
  }
  public static void main(String[] args) {
    int num1 = (int)(1000 * Math.random()), num2 = (int)(1000 * Math.random()), num3 = (int)(1000 * Math.random());
    System.out.println(num1 + " " + num2 + " " + num3);
    System.out.println(hasSameLastDigit(num1, num2, num3));
  }
}
