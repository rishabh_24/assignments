package assignments.Basic_Java;

public class Palindrome {
  public static boolean isPalindrome(int num){
    int revNum = 0, temp = num;
    while(temp != 0){
      revNum = revNum * 10 + temp % 10;
      temp /= 10;
    }
    return revNum == num;
  }
  public static void main(String[] args) {
    System.out.println(isPalindrome((int)(11211)));
    return;
  }
}
