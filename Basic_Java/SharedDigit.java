package assignments.Basic_Java;

public class SharedDigit {
  public static boolean hasSharedDigit(int num1, int num2){
    while(num1 != 0){
      int temp = num2;
      while(temp != 0){
        if(temp % 10 == num1 % 10){
          return true;
        }
        temp /= 10;
      }
      num1 /= 10;
    }
    return false;
  }
  public static void main(String[] args) {
    int num1 = (int)(1000 * Math.random()), num2 = (int)(9000 * Math.random());
    System.out.println(num1 +" "+num2);
    System.out.println(hasSharedDigit(num1, num2));
  }
}
