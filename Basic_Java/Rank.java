package assignments.Basic_Java;
public class Rank {
  public static void main(String[] args){
    String name = "Tony";
    int score = 739;
    int position = calculateHighScore(score);
    displayHighScorePosition(name, position);
    return;
  }
  public static void displayHighScorePosition(String name, int position){
    System.out.println(name + " managed to get into position : " + position + " on high score table!");
  }
  public static int calculateHighScore(int score){
    int position;
    if(score >= 1000){
      position = 1;
    } else if(score >= 500){
      position = 2;
    } else if(score >= 100){
      position = 3;
    } else {
      position = 4;
    }
    return position;
  }
}
