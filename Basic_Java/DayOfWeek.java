package assignments.Basic_Java;

public class DayOfWeek {
  public static void printDayOfTheWeek(int day){
    String s;
    switch(day) {
      case 0 :
        s = "Sunday";
        break;
      case 1:
        s = "Monday";
        break;
      case 2:
        s = "Tuesday";
        break;
      case 3:
        s = "Wednesday";
        break;
      case 4:
        s = "Thursday";
        break;
      case 5:
        s = "Friday";
        break;
      case 6:
        s = "Saturday";
        break;
      default :
        s = "Invalid day";
        break;
    }
    System.out.println(s);
  }
  public static void main(String[] args) {
    printDayOfTheWeek(6);
  }
}
