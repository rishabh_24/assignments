package assignments.Basic_Java;

public class FlourPacker {
  public static boolean canPack(int big, int small, int target){
    int total = 5 * big + small;
    if(target < 0){
      return false;
    }
    if(total < target) {
      return false;
    } else if(total == target) {
      return true;
    } else {
      int left = target % 5;
      int temp = target - left;
      if(big >= target / 5){
        target -= temp;
        if(small >= target){
          return true;
        }
        return false;
      } else {
        target -= 5 * big;
        if(small >= target){
          return true;
        }
        return false;
      }
    }
  }
  public static void main(String[] args) {
    System.out.println(canPack((int)(1000 * Math.random()), (int)(1000 * Math.random()), (int)(1000 * Math.random())));
    return;
  }
}
