package assignments.Basic_Java;

public class SummOdd {
  public static boolean isOdd(int num){
    return num % 2 == 1;
  }
  public static int sumOdd(int lower, int upper){
    int sum = 0;
    for(int i = lower; i <= upper; i++){
      sum += isOdd(i) ? i : 0;
    }
    return sum;
  }
  public static void main(String[] args) {
    int lower = (int)(100 * Math.random()), upper = (int)(1000 * Math.random());
    if(lower > upper) {
      lower = lower ^ upper;
      upper = lower ^ upper;
      lower = lower ^ upper;
    }
    System.out.println(sumOdd(lower, upper));
    return;
  }
}
