package assignments.Basic_Java;

public class GCD {
  public static int getGreatestCommonDivisor(int a, int b) {
    if(a < 10 || b < 10){
      return -1;
    }
    for(int i = Math.min(a, b); i >= 1; i--){
      if(a % i == 0 && b % i == 0){
        return i;
      }
    }
    return 1;
  }
  public static void main(String[] args) {
    int num1 = (int)(1000 * Math.random()), num2 = (int)(9000 * Math.random());
    System.out.println(num1 +" "+num2);
    System.out.println(getGreatestCommonDivisor(num1, num2));
    return;
  }
}
