package assignments.Basic_Java;
public class LeapYear {
  public static boolean isLeapYear(int year){
    if((year % 100) == 0 && (year % 400) == 0){
      return true;
    }
    if((year % 4) == 0){
      return true;
    }
    return false;
  }
  public static void main(String[] args){
    int year = (int)(1000 * Math.random());
    System.out.println(year);
    System.out.println(isLeapYear(year));
    return;
  }
}
