package assignments.Basic_Java;

public class PrimeFActor {
  public static void primeFactors(int num) {
    for(int i = 1; i <= num >> 1; i++){
      if(num % i == 0){
        System.out.println(i);
      }
    }
    System.out.println(num);
  }
  public static void main(String[] args) {
    int num = (int)(1000 * Math.random());
    System.out.println(num);
    primeFactors(num);
    return;
  }
}
