package assignments.Basic_Java;

import java.util.Scanner;

public class InputCalc {
  public static void InputThenPrintSumAndAverage(){
    Scanner scanner = new Scanner(System.in);
    int sum = 0, avg = 0, cnt = 0;
    while(true){
      boolean isInt = scanner.hasNextInt();
      if(isInt){
        int num = scanner.nextInt();
        sum += num;
        cnt++;
        avg = (int)Math.ceil((double)sum / (double)cnt);
      } else {
        System.out.println("SUM = " + sum + " AVG = " + avg);
        break;
      }
    }
    scanner.close();
    return;
  }
  public static void main(String[] args) {
    InputThenPrintSumAndAverage();
  }
}
