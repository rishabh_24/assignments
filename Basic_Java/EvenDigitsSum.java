package assignments.Basic_Java;

public class EvenDigitsSum {
  public static int getEvenDigitSum(int num){
    if(num < 0){
      return -1;
    }
    int sum = 0;
    while(num != 0){
      if(num % 2 == 0){
        sum += num % 10;
      }
      num /= 10;
    }
    return sum;
  }
  public static void main(String[] args) {
    int num = (int)(10000 * Math.random());
    System.out.println(num);
    System.out.println(getEvenDigitSum(num));
    return;
  }
}
