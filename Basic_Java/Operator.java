package assignments.Basic_Java;

public class Operator {
  public static void main(String[] args){
    double var1 = 20d;
    double var2 = 80d;
    double mul = 100d * var2 + var1;
    double rem = mul % 40d;
    boolean isZero = (rem == 0) ? true : false;
    if(!isZero){
      System.out.println("Got some reminder!");
    }
  }
}
