package assignments.Basic_Java;

public class PerfectNumber {
  public static boolean isPerfectNumber(int num) {
    if(num < 1){
      return false;
    }
    int sum = 0;
    for(int i = 1; i <= num >> 1; i++){
      if(num % i == 0){
        sum += i;
      }
    }
    return sum == num;
  }
  public static void main(String[] args) {
    int num = (int)(1000 * Math.random());
    System.out.println(isPerfectNumber(num));
    return;
  }
}
