package assignments.Basic_Java;

public class LargestPrime {
  public static int getLargestPrime(int num){
    if(num <= 1){
      return -1;
    }
    boolean flag = false;
    int ans = 0;
    for(int i = num; i >= 1; i--){
      flag = false;
      for(int j = 2; j * j <= i; j++){
        if(i % j == 0){
          flag = true;
          break;
        }
      }
      if(!flag && num % i == 0){
        ans = i;
        break;
      }
    }
    return ans;
  }
  public static void main(String[] args) {
    int num = (int)(1000 * Math.random());
    System.out.println(num);
    System.out.println(getLargestPrime(7));
  }
}
