package assignments.Basic_Java;

public class FeetAndInchesToCentimeter {
  public static double calcFeetAndInchesToCentimeters(int feet, int inches){
    if(feet < 0 || (inches < 0 || inches > 12)){
      System.out.println("Invalid feet or inches");
      return -1;
    }
    double inCentimeters = (inches * 2.54) + (feet * 12 * 2.54);
    System.out.println(feet + " feet, " + inches + " inches = " + inCentimeters + " cm");
    return inCentimeters;
  }
  public static double calcFeetAndInchesToCentimeters(int inches){
    if(inches < 0 || inches > 12){
      return -1;
    }
    return inches / 12 + inches % 12;
  }
  public static void main(String[] args){
    calcFeetAndInchesToCentimeters(6, 13);
  }
}
