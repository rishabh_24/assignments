package assignments.Basic_Java;

public class Prime {
  public static boolean isPrime(int num){
    for(int i = 2; i * i <= num ; i++){
      if(num % i == 0){
        return false;
      }
    }
    return true;
  }
  public static void main(String[] args) {
    int upper = (int)(1000 * Math.random());
    int cnt = 0;
    for(int i = 2; i <= upper; i++){
      cnt += isPrime(i) ? 1 : 0;
    }
    System.out.println(cnt);
  }
}
