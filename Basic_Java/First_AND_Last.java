package assignments.Basic_Java;

public class First_AND_Last {
  public static int sumFirstAndLastDigit(int num){
    if(num < 0){
      return -1;
    }
    int last = 0, first = 0;
    boolean flag = true;
    while(num != 0){
      if(flag){
        last = num % 10;
        flag = false;
      }
      if(num - num % 10 == 0){
        first = num % 10;
      }
      num /= 10;
    }
    return first + last;
  }
  public static void main(String[] args) {
    int num = (int)(1000 * Math.random());
    System.out.println(num);
    System.out.println(sumFirstAndLastDigit(num));
    return;
  }
}
