package assignments.Basic_Java;

public class MegabytConverter {
  public static void printMegaBytesAndKiloBytes(int KB){
    if(KB < 0){
      System.out.println("Invalid Value");
      return;
    }
    int remKB = KB % 1024;
    int MB = KB / 1024;
    System.out.println(KB +" KB = " + MB +" MB and " + remKB + " KB");
  }
  public static void main(String[] args){
    printMegaBytesAndKiloBytes(1025);
  }
}
