package assignments.OOPS.EXERCISE_38;

public class Printer {
  private int tonerLevel;
  private int pagesPrinted;
  private boolean duplex;

  public Printer(int tonerLevel, boolean duplex) {
      this.duplex = duplex;
      this.pagesPrinted = 0;
      if (tonerLevel > -1 && tonerLevel <= 100) {
          this.tonerLevel = tonerLevel;
      } else {
          this.tonerLevel = -1;
      }
  }

  public int addToner(int tonerAmount) {
      if (tonerAmount > 0 && tonerAmount <= 100) {
          if (tonerAmount + this.tonerLevel > 100) {
              return -1;
          } 
          else {
              this.tonerLevel += tonerAmount;
              return this.tonerLevel;
          }
      } 
      else {
          return -1;
      }
  }

  public int printPages(int pages) {
      int pagesToPrint = pages;
      if (this.duplex) {
          System.out.println("Printing in duplex mode");
          pagesToPrint /= 2;
          if (pages % 2 == 1) {
              pagesToPrint += 1;
          }
          this.pagesPrinted += pagesToPrint;
      }
      return pagesToPrint;
  }

  public int getPagesPrinted() {
      return this.pagesPrinted;
  }

  public static void main(String[] args) {
      Printer printer = new Printer(50, true);
      System.out.println(printer.addToner(50));
      System.out.println(printer.getPagesPrinted());
      int pagesPrinted = printer.printPages(4);
      System.out.println(" " + pagesPrinted + " " + printer.getPagesPrinted());
      pagesPrinted = printer.printPages(2);
      System.out.println(" " + pagesPrinted + " " + printer.getPagesPrinted());
  }
}
