package assignments.OOPS.EXERCISE_34;

public class ComplexNumber {
  private double real, imaginary;
  public ComplexNumber(double a, double b) {
    this.real = a;
    this.imaginary = b;
  }
  public double getImaginary() {
    return imaginary;
  }
  public double getReal(){
    return real;
  }
  public void add(double a, double b) {
    this.real += a;
    this.imaginary += b;
  }
  public void subtract(double a, double b) {
    this.real -= a;
    this.imaginary -= b;
  }
  public void add(ComplexNumber z){
    this.real += z.getReal();
    this.imaginary += z.getImaginary();
  }
  public void subtract(ComplexNumber z){
    this.real -= z.getReal();
    this.imaginary -= z.getImaginary();
  }
}
