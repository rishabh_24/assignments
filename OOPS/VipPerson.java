package assignments.OOPS;

public class VipPerson {
  private String name;
  private String emailAddress;
  private double creditLimit;
  public VipPerson(){
    this("Default name", "d@gmail.com", 200000.00);
  }
  public VipPerson(String name, String emailAddress, double creditLimit){
    this.name = name;
    this.emailAddress = emailAddress;
    this.creditLimit = creditLimit;
  }
  public VipPerson(String name, double creditLimit){
    this(name, "uk@gmail.com", creditLimit);
  }
  public String getName(){
    return this.name;
  }
}
