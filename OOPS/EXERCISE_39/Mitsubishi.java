package assignments.OOPS.EXERCISE_39;

public class Mitsubishi extends Car {
  public Mitsubishi(int cylinders, String name) {
    super(cylinders, name);
  }
  public String startEngine(){
    return "Mitsubishi -> startedEngine()";
  }
  public String accelarate(){
    return "Mitsubishi -> accelarate()";
  }
  public String brake(){
    return "Mitsubishi -> brakes()";
  }
}
