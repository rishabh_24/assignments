package assignments.OOPS.EXERCISE_39;

public class Car {
  private boolean engine;
  private int cylinders, wheels;
  private String name;
  
  public Car(int cylinders, String name) {
    this.engine = true;
    this.cylinders = cylinders;
    this.wheels = 4;
    this.name = name;
  }

  public String startEngine(){
    return "Car -> startedEngine()";
  }
  public String accelarate(){
    return "Car -> accelarate()";
  }
  public String brake(){
    return "Car ->brakes()";
  }
  public int getCylinders() {
    return this.cylinders;
}

public String getName() {
    return this.name;
}
}
