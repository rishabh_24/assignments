package assignments.OOPS.EXERCISE_39;

public class Holden extends Car {
  public Holden(int cylinders, String name) {
   super(cylinders, name);
 }
 public String startEngine(){
   return "Holden -> startedEngine()";
 }
 public String accelarate(){
   return "Holden -> accelarate()";
 }
 public String brake(){
   return "Holden -> brakes()";
 }
}
