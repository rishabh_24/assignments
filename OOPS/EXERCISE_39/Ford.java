package assignments.OOPS.EXERCISE_39;

public class Ford extends Car {
  public Ford(int cylinders, String name) {
   super(cylinders, name);
 }
 public String startEngine(){
   return "Ford -> startedEngine()";
 }
 public String accelarate(){
   return "Ford -> accelarate()";
 }
 public String brake(){
   return "Ford -> brakes()";
 }
}
