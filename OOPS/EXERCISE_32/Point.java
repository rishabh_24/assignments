package assignments.OOPS.EXERCISE_32;

public class Point {
  private int x, y;
  public Point(){
    System.out.println("Constructor invoked!");
  }
  public Point(int x, int y){
    setX(x);
    setY(y);
  }
  public int getX(){
    return x;
  }
  public int getY(){
    return y;
  }
  public void setY(int y){
    this.y = y;
  }
  public void setX(int x){
    this.x = x;
  }
  public double distance(){
    return Math.sqrt(x * x + y * y);
  }
  public double distance(int a, int b){
    int newx = x - a;
    int newy = y - b;
    return Math.sqrt(Math.pow(newx, 2) + Math.pow(newy, 2));
  }
  public double distance(Point obj){
    int newx = x - obj.getX();
    int newy = y - obj.getY();
    return Math.sqrt(Math.pow(newx, 2) + Math.pow(newy, 2));
  }
}
