package assignments.OOPS.EXERCISE_31;

public class Wall {
  private double width;
  private double height;

  public Wall() {
    System.out.println("WALL CONSTRUCTOR");
  }
  public Wall(double width, double height) {
    setWidth(width);
    setHeight(height);
  }
  public double getWidth() {
    return width;
  }
  public double getHeight(){
    return height;
  }
  private void setWidth(double width){
    this.width = width < 0 ?  0 : width;
  }
  private void setHeight(double height){
    this.height = height < 0 ? 0 : height;
  }
  public double getArea() {
    return width * height;
  }
}
