package assignments.OOPS;

public class Person {
  String firstName;
  String lastName;
  int age;
  public void setFirstName(String name) {
    this.firstName = name;
  }
  public void setLastName(String name) {
    this.lastName = name;
  }
  public void setAge(int age) {
    this.age = (age > 100 || age < 0) ? 0 :age;
  }
  public String getFirstName() {
    return this.firstName;
  }
  public String getLastName() {
    return this.lastName;
  }
  public int getAge() {
    return this.age;
  }
  public boolean isTeen(){
    int age = this.age;
    return age > 12 && age < 20 ? true : false;
  }
  public String getFullName() {
    String s;
    if(this.firstName.isEmpty() && this.lastName.isEmpty()){
      s = "";
    } else if(this.firstName.isEmpty()){
      s = this.lastName;
    } else if (this.lastName.isEmpty()){
      s = this.firstName;
    } else {
      s = this.firstName + " " + this.lastName;
    }
    return s;
  }
}
