package assignments.OOPS;

public class Account {
  private String accNumber;
  private long balance;
  private String name;
  private String email;
  private String phone;

  public Account(String accNumber, long balance, String name, String email, String phone) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.accNumber = accNumber;
    this.balance = balance;
  }
  public String getEmail() {
    return this.email;
  }
  public String getPhone() {
    return this.phone;
  }
  public String getAccNumber() {
    return this.accNumber;
  }
  public double withdrawl(double amount) {
    if(this.balance - amount < 0){
      System.out.println("Insufficient funds!");
    }
    return this.balance -= amount;
  }
  public double deposit(double amount) {
    System.out.println("NEW :" + (this.balance + amount));
    return this.balance += amount;
  }
}
