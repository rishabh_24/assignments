package assignments.OOPS.EXERCISE_37;

public class Ceiling {
  private int height;
  private int paintedColor;

  public Ceiling(int height, int paint){
    this.height = height;
    this.paintedColor = paint;
  }

  public int getHeight(){
    return height;
  }

  public int getPaintedColor(){
    return paintedColor;
  }
}
