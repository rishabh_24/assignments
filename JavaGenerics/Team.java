package assignments.JavaGenerics;

import java.util.ArrayList;
import java.util.List;

public class Team <T extends Player> implements Comparable<Team<T>>{
  private String name;
  int win = 0, lost = 0, draw = 0, played = 0;
  private List<T> members = new ArrayList<>();
  public Team(String name) {
    this.name = name;
  }
  public String getName() {
    return name;
  }
  public boolean addPlayer(T player) {
    if(members.contains(player)){
      System.out.println(player.getName() + " is alreadyd on team");
      return false;
    }
    members.add(player);
    System.out.println(player.getName() + " picked for the team " +this.name);
    return true;
  }

  public int numSize(){
    return this.members.size();
  }

  public void matchResult(Team<T> team, int ourScore, int theirScore){
    if(ourScore > theirScore){
      win++; played++;
    } else if( theirScore > ourScore){
      lost++; played++;
    } else{
      draw++; played++;
    }
    if(team != null){
      matchResult(null, theirScore, ourScore);
    }
  }
  public int ranking(){
    return (win * 2) + draw;
  }

  @Override
  public int compareTo(Team<T> team){
    if(this.ranking() > team.ranking()){
      return -1;
    } else if(this.ranking() < team.ranking()){
      return 1;
    }
    return 0;
  }
}
