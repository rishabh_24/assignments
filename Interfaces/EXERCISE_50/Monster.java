package assignments.Interfaces.EXERCISE_50;

import java.util.ArrayList;
import java.util.List;

public class Monster implements ISaveable {
  private String name;
  private int hitPoints;
  private int strength;

  public Monster(String name, int hitPoints, int strength){
    this.name = name;
    this.hitPoints = hitPoints;
    this.strength = strength;
  }

  public String getName(){
    return name;
  }

  public int getHitPoints(){
    return hitPoints;
  }

  public int getStrength(){
    return strength;
  }

  @Override
  public List<String> write() {
    List <String> result = new ArrayList<String>();
    result.add(0, this.name);
    result.add(1, "" + this.hitPoints);
    result.add(2, "" + this.strength);
    return result;
  }

  @Override
  public void read(List<String> savedValues) {
    this.name = savedValues.get(0);
    this.hitPoints = Integer.parseInt(savedValues.get(1));
    this.strength = Integer.parseInt(savedValues.get(2));
  }

  @Override
  public String toString(){
    return "Player{" +
    "name=" + name + '\'' +
    ", hitPoints=" + hitPoints +'\'' +
    ", strength=" + strength + 
    "}";
  }
}
