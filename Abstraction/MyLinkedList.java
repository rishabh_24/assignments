package assignments.Abstraction;

public class MyLinkedList implements NodeList{

  private ListItem root = null;

  public MyLinkedList(ListItem object) {
    this.root = object;
  }

  @Override
  public ListItem getRoot() {
    return this.root;
  }

  @Override
  public boolean addItem(ListItem item) {
    if(this.root == null){
      this.root = item;
      return true;
    }
    ListItem currentItem = this.root;
    while(currentItem != null){
      int comparison = currentItem.compareTo(item);
      if(comparison < 0){
        if(currentItem.next() != null){
          currentItem = currentItem.next();
        } else {
          currentItem.setNext(item);
          item.setPrevious(currentItem);
          return true;
        }
      } else if(comparison > 0){
        if(currentItem.previous() != null){
          currentItem.previous().setNext(item);
          item.setPrevious(currentItem.previous());
          item.setNext(currentItem);
          currentItem.setNext(item);
        } else {
          item.setNext(currentItem);
          currentItem.setPrevious(item);
          this.root = item;
          return true;
        }
      } else {
        System.out.println(item.getValue() + " is already present, not added.");
        return false;
      }
    }
    return false;
  }

  @Override
  public boolean removeItem(ListItem item) {
    if(item != null){
      System.out.println("Deleting item " + item.getValue());
      ListItem currentItem = this.root;
      while(currentItem != null){
       int comparison = currentItem.compareTo(item);
        if(comparison == 0){
          if(currentItem == this.root){
            this.root = currentItem.next();
          } else {
            currentItem.previous().setNext(currentItem.next());
            if(currentItem.next() != null){
              currentItem.next().setPrevious(currentItem.previous());
            }
          }
          return true;
        } else if(comparison < 0){
          currentItem = currentItem.next();
        } else {
          break;
        }
      }
    }
    System.out.println("No such item exist!");
    return false;
  }

  @Override
  public void traverse(ListItem root) {
    if(root == null) {
      System.out.println("This list is empty");
      return;
    }
    while(root != null) {
      System.out.println(root.getValue());
      root = root.next();
    }
    return;
  }
  
}
